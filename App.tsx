import { View } from 'react-native';
import { MapViewRender } from './src/components/MapViewRender';
import { global } from './src/styles/global';

export default function App() {
  return (
    <View style={global.container}>
      <MapViewRender />
    </View>
  );
}
